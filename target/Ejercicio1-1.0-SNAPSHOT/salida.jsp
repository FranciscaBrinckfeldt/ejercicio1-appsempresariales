<%-- 
    Document   : salida
    Created on : 28-03-2020, 18:59:59
    Author     : Francisca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <link rel="stylesheet" type="text/css" href="style.css"/>
        <title>Resultado Ejercicio 1</title>
    </head>
    <body>
        <div class="generalcontainer">
            <div class="title">
                <h1 align="center">Taller de Aplicaciones Empresariales</h1>
                <h1 align="center">Actividad sumativa - Ejercicio 1</h1>
            </div>


            <% 
                String name = (String) request.getAttribute("name");
                String seccion = (String) request.getAttribute("seccion");
            %>

            <div class="respuestacontainer">
                <div class="columns is-mobile">
                    <div class="column is-half is-offset-one-quarter">
                        <div class="content">
                            <h3 align="center">Hola <%=name%>, tu sección es la <%=seccion%></h3>
                        </div>
                    </div>
                </div>
            </div>
            
            <footer>
                <div class="presentacioncontainer">
                    <h4 align="center">Alumna: Francisca Brinckfeldt Fernández.</h4>
                </div>
            </footer>
                        
        </div>
    </body>
</html>
