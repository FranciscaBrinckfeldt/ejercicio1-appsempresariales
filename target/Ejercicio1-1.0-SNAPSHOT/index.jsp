<%-- 
    Document   : index
    Created on : 28-03-2020, 15:04:16
    Author     : Francisca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <link rel="stylesheet" type="text/css" href="style.css"/>
        <title>Ejercicio 01</title>
    </head>
    <body>
        <div class="generalcontainer">
            <div class="title">
                <h1 align="center">Taller de Aplicaciones Empresariales</h1>
                <h1 align="center">Actividad sumativa - Ejercicio 1</h1>
            </div>

            <div class="subtitle">
                <h2 align="center">Ingresa los siguientes datos:</h2>
            </div>

            <form name="form" action="resultado" method="POST">
                <div class="container">
                    <div class="columns is-mobile">
                        <div class="column is-three-fifths is-offset-one-fifth">
                            
                            <div class="inputcontainer">
                                <div class="field">
                                    <label class="label">Nombre:</label>
                                    <div class="control">
                                      <input class="input" type="text" name="name" placeholder="Ingresa tu nombre">
                                    </div>  
                                </div>
                             </div>

                            <div class="inputcontainer">
                                <div class="field">
                                    <label class="label">Sección:</label>
                                    <div class="control">
                                      <input class="input" type="number" name="seccion" placeholder="Ingresa tu sección">
                                    </div>
                                 </div>
                            </div>
                            
                            <div class="field">
                                <div class="control">
                                    <div class="buttoncontainer">
                                        <input class="button is-link" type="submit" value="Enviar">
                                    </div>
                                </div>
                            </div> 
                            
                        </div>
                    </div>
                </div>
            </form>
            
            <footer>
                <div class="presentacioncontainer">
                    <h4 align="center">Alumna: Francisca Brinckfeldt Fernández.</h4>
                </div>
            </footer>
            
        </div>
    </body>
</html>
